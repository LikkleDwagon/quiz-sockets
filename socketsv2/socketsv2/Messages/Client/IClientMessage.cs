﻿namespace socketsv2.Messages.Client
{
    public enum ClientMessageType
    {
        Answer, NextRound
    }

    public interface IClientMessage
    {
        ClientMessageType Type { get; }
    }
}