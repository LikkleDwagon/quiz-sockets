﻿using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using socketsv2.Messages.Client;

namespace socketsv2.Processors
{
    public interface IProcessor<T>
    where T : IClientMessage
    {
        Task Process(HttpRequest request, T message, string sessionName);
    }

    public abstract class BaseProcessor
    {
        public static void SendPlayers(string sessionName, object message)
        {
            StartupExtensions.GetPlayerSockets(sessionName).Select(s => s.Value).ToList().ForEach(s => { SendMessage(message, s); });
        }

        public static void SendMessage(object message, WebSocket s)
        {
            var serializedString = JsonConvert.SerializeObject(message);
            s.SendAsync(ObjectToByteArray(serializedString), WebSocketMessageType.Text, true,
                CancellationToken.None);
        }

        public static void SendHost(string sessionName, object message)
        {
            if (!StartupExtensions.HostSockets.ContainsKey(sessionName))
                return;

            var serializedString = JsonConvert.SerializeObject(message);
            StartupExtensions.HostSockets[sessionName].SendAsync(ObjectToByteArray(serializedString), WebSocketMessageType.Text, true, CancellationToken.None);
        }

        public static void SendAll(string sessionName, object message)
        {
            SendPlayers(sessionName, message);
            SendHost(sessionName, message);
        }


        private static byte[] ObjectToByteArray(string obj)
        {
            return Encoding.UTF8.GetBytes(obj);
        }
    }
}