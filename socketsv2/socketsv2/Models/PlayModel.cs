﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using socketsv2.SqlServer;

namespace socketsv2.Models
{
    public class PlayModel
    {
        public string Name { get; set; }
        public List<string> ConnectedPlayers { get; set; }
        public QuizContext.LeaderBoard LeaderBoard { get; set; }
        public string SessionName { get; internal set; }
    }

    public class PlayerModel : PlayModel
    {
        public int Score { get; set; }
        public long Rank { get; set; }
        public double Percent { get; set; }
    }
}
