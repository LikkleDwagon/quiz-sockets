﻿namespace socketsv2.Messages
{
    public enum MessageType
    {
        Connect,
        Answer,
        DisConnect,
        PlayerReady,
        NextRound,
        YourScore
    }

    public abstract class SocketMessage
    {
        public abstract MessageType Type { get; }
    }
}