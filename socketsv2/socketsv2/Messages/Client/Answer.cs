﻿namespace socketsv2.Messages.Client
{
    public class Answer : IClientMessage
    {
        public string Option { get; set; }
        public ClientMessageType Type { get; set; }
    }
}
