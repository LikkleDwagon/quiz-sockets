﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace socketsv2.SqlServer.Models
{
    public class Session
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Round> Rounds { get; set; }

        public Session()
        {
            Rounds = new HashSet<Round>();
        }
    }

    public class Round
    {
        public Round()
        {
            Correct = new HashSet<PlayerName>();
            Wrong = new HashSet<PlayerName>();
        }

        [Key]
        public int Id { get; set; }

        public System.DateTime date { get; set; }
        public int RoundNumber { get; set; }

        public virtual ICollection<PlayerName> Correct { get; set; }

        public virtual ICollection<PlayerName> Wrong { get; set; }
    }

    public class PlayerName
    {
        [Key]
        public int Id { get; set; }        
        public string Name { get; set; }
    }
}