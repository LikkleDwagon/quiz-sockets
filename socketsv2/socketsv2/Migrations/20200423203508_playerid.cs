﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace socketsv2.Migrations
{
    public partial class playerid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayerName",
                table: "PlayerName");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "PlayerName",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "PlayerName",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayerName",
                table: "PlayerName",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayerName",
                table: "PlayerName");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "PlayerName");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "PlayerName",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayerName",
                table: "PlayerName",
                column: "Name");
        }
    }
}
