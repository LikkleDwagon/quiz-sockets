﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using socketsv2.Models;
using socketsv2.SqlServer;

namespace socketsv2.Controllers
{
    public class HostController : Controller
    {
        private readonly QuizContext _context;

        public HostController(QuizContext context)
        {
            _context = context;
        }

        // GET
        public async Task<IActionResult> Index()
        {
            return View("StartSession", new HostModel());
        }

        [HttpPost]
        public async Task<IActionResult> Host(HostModel model)
        {
            var guid = Guid.NewGuid().ToString();
            //Response.Cookies.Append("name", guid);
            await _context.CreateSessionIfNotExists(model.SessionName);

            var leaderBoard = await _context.GetLeaderboard(model.SessionName);
            var connectedPlayers = StartupExtensions.GetPlayerSockets(model.SessionName).Select(s => s.Key).ToList();
            return View(new PlayModel()
            {
                Name = guid,
                ConnectedPlayers = connectedPlayers,
                LeaderBoard = leaderBoard,
                SessionName = model.SessionName
            });
        }
    }

    public class HostModel
    {
        public string SessionName { get; set; }
    }

}