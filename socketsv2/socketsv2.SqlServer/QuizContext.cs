﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using socketsv2.SqlServer.Models;

namespace socketsv2.SqlServer
{
    public class QuizContext : DbContext
    {
        public QuizContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Round> Rounds { get; set; }
        public DbSet<Session> Sessions {get;set;}
        
        public async Task<LeaderBoard> GetLeaderboard(string sessionName)
        {
            var session = Sessions.Where(s => s.Name == sessionName);
            if (session == null) return null;

            var scores = await GetRounds(sessionName)
                .SelectMany(r => r.Correct)
                .GroupBy(c => c.Name)
                .Select(grouping => new Score()
                    {
                        Name = grouping.Key,
                        Amount = grouping.Count()
                    })
            .OrderByDescending(s => s.Amount)
            .ThenBy(r => r.Name)
            .Take(3)
            .ToListAsync();

            return new LeaderBoard(){Scores = scores};
        }

        public async Task<bool> CreateSessionIfNotExists(string sessionName)
        {
            if (await Sessions.AnyAsync(s => s.Name == sessionName))
                return false;

            var session = new Session() { Name = sessionName };
            await Sessions.AddAsync(session);
            await SaveChangesAsync();
            return true;
        }


        public async Task<int> GetScore(string name, string sessionName)
        {
            return await GetRounds(sessionName).SelectMany(r => r.Correct).Where(s => s.Name == name).CountAsync();
        }

        public IQueryable<Round> GetRounds(string sessionName)
        {
            return Sessions.Where(s => s.Name == sessionName).SelectMany(r => r.Rounds);
        }

        public async Task<double> GetCorrectPercent(string name, string sessionName)
        {
            // todo session
            var correct = await GetRounds(sessionName).Where(r => r.Correct.Any(c => c.Name == name)).CountAsync();
            var wrong = await GetRounds(sessionName).Where(r => r.Wrong.Any(c => c.Name == name)).CountAsync();

            return Math.Round(((float)correct / (correct + wrong)) * 100, 2);
        }

        public async Task<long> GetRank(string name, string sessionName)
        {
            // todo session
            var grouping = await GetRounds(sessionName)
                .SelectMany(r => r.Correct)
                .GroupBy(c => c.Name.ToLower())
                .OrderByDescending(g => g.Count())
                .ThenBy(r => r.Key)
                .ToListAsync();

                var scores = grouping.Select((gr, i) => new 
                {
                    Name = gr.Key,
                    Rank = i + 1
                });

            var knownRank =  scores.SingleOrDefault(u => u.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))?.Rank;
            if (knownRank.HasValue)
                return knownRank.Value;

            if (!scores.Any())
                return 1;

            return scores.Max(g => g.Rank) + 1;
        }

        public class LeaderBoard
        {
            public List<Score> Scores { get; set; }
        }

        public class Score
        {
            public string Name { get; set; }
            public int Amount { get; set; }
        }
    }
}
