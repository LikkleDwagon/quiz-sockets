﻿namespace socketsv2.Messages
{
    public class YourScoreMessage : SocketMessage
    {
        public override MessageType Type => MessageType.YourScore;
        public int Score { get; }
        public long Rank { get; }
        public double Percent { get; }

        public YourScoreMessage(int score, long rank, double percent)
        {
            Score = score;
            Rank = rank;
            Percent = percent;
        }
    }
}
