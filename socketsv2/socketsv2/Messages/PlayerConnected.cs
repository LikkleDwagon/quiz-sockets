﻿namespace socketsv2.Messages
{
    public class PlayerConnected : SocketMessage
    {
        public string Name { get; }

        public PlayerConnected(string name)
        {
            Name = name;
        }

        public override MessageType Type => MessageType.Connect;
    }

    public class PlayerDisconnected : PlayerConnected
    {
        public PlayerDisconnected(string name) : base(name)
        {
        }

        public override MessageType Type => MessageType.DisConnect;
    }
}