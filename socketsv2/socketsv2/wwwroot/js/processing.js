﻿function ProcessConnect(message) {

    var disconnectedPlayers = document.querySelectorAll('.disconnected');
    for (let i = 0; i < disconnectedPlayers.length; i++) {
        disconnectedPlayers[i].remove();
    };
    
    const element = document.createElement("p");
    element.id = message.Name;
    element.innerHTML = message.Name;
    element.classList.add("connected");
    element.setAttribute("data-player", message.Name);
    document.getElementById('players').append(element);

}

function ProcessDisconnect(message) {
    const playerElements = document.querySelectorAll(`[data-player="${message.Name}"]`);
    for (let i = 0; i < playerElements.length; i++) {
        playerElements[i].classList.remove("connected");
        playerElements[i].classList.add("disconnected");
        playerElements[i].attributes.removeNamedItem("data-player");
    }
}

function ProcessNextRound(message) {
    document.querySelector("#questionNumber").innerHTML = message.Round + 1;
    const roundP = document.createElement("p");
    roundP.innerHTML = message.Round + ": ";
    if (message.Correct !== null && message.Correct !== undefined) {
        for (let i = 0; i < message.Correct.length; i++) {
            roundP.innerHTML += message.Correct[i] + " ";
        }
    }
    document.querySelector("#scoresContent").prepend(roundP);

    if (message.LeaderBoard !== null && message.LeaderBoard !== undefined) {
        if (message.LeaderBoard.Scores.length >= 1) {
            document.getElementById("position1").innerHTML =
                `${message.LeaderBoard.Scores[0].Name}: ${message.LeaderBoard.Scores[0].Amount}`;
        }
        if (message.LeaderBoard.Scores.length >= 2) {
            document.getElementById("position2").innerHTML =
                `${message.LeaderBoard.Scores[1].Name}: ${message.LeaderBoard.Scores[1].Amount}`;
        }
        if (message.LeaderBoard.Scores.length >= 3) {
            document.getElementById("position3").innerHTML =
                `${message.LeaderBoard.Scores[2].Name}: ${message.LeaderBoard.Scores[2].Amount}`;
        }
    }
}

function sendMessage(socket, message) {
    const json = JSON.stringify(message);
    socket.send(encodeURIComponent(json));
}