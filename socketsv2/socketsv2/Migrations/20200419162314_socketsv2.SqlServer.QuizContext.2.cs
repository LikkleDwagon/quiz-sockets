﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace socketsv2.Migrations
{
    public partial class socketsv2SqlServerQuizContext2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Scores",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Round",
                table: "Scores",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "Scores");

            migrationBuilder.DropColumn(
                name: "Round",
                table: "Scores");
        }
    }
}
