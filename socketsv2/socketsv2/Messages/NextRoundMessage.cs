﻿using socketsv2.SqlServer;

namespace socketsv2.Messages
{
    public class NextRoundMessage : SocketMessage
    {
        public override MessageType Type => MessageType.NextRound;
        public int Round { get; set; }
        public string[] Correct { get; set; }
        public QuizContext.LeaderBoard LeaderBoard { get; set; }
    }
}