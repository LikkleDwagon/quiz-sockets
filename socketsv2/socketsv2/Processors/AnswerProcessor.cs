﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using socketsv2.Messages;
using socketsv2.Messages.Client;

namespace socketsv2.Processors
{
    public class AnswerProcessor : BaseProcessor, IProcessor<Answer>

    {
        public async Task Process(HttpRequest request, Answer answer, string sessionName)
        {
            var playerAnswer = new PlayerAnswer()
            {
                Player = request.Cookies["name"],
                Option = answer.Option
            };

            SendHost(sessionName, playerAnswer);
            SendPlayers(sessionName, new PlayerReady(){Name= request.Cookies["name"]});
        }
    }
}