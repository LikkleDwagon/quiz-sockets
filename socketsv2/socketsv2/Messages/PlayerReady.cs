﻿namespace socketsv2.Messages
{
    public class PlayerReady : SocketMessage
    {
        public override MessageType Type => MessageType.PlayerReady;

        public string Name { get; set; }
    }
}