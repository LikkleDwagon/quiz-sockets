﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using socketsv2.Messages;
using socketsv2.Messages.Client;
using socketsv2.SqlServer;
using socketsv2.SqlServer.Models;

namespace socketsv2.Processors
{
    public class NextRoundProcessor : BaseProcessor, IProcessor<NextRound>
    {
        private readonly QuizContext _db;

        public NextRoundProcessor(QuizContext db)
        {
            _db = db;
        }

        public async Task Process(HttpRequest request, NextRound message, string sessionName)
        {
            var openSockets = StartupExtensions.GetPlayerSockets(sessionName).Where(s => s.Value.State == System.Net.WebSockets.WebSocketState.Open).ToList();
            var session = _db.Sessions.SingleOrDefault(s => s.Name == sessionName);
            var round = new Round()
            {
                date = DateTime.Now,
                RoundNumber = message.RoundNumber,
            };

            openSockets.ForEach(o =>
            {
                var playerName = new PlayerName() { Name = o.Key };
                if (message.Correct.Any(c => c.ToLower() == o.Key.ToLower()))
                {
                    round.Correct.Add(playerName);
                }
                else
                {
                    round.Wrong.Add(playerName);
                }
            });

            session.Rounds.Add(round);

            await _db.SaveChangesAsync();

            var leaderBoard = await _db.GetLeaderboard(sessionName);

            SendAll(sessionName, new NextRoundMessage(){Round = message.RoundNumber, Correct =message.Correct, LeaderBoard = leaderBoard});

            foreach(var socket in openSockets)
            {
                var score = await _db.GetScore(socket.Key, sessionName);
                var rank = await _db.GetRank(socket.Key, sessionName);
                var percent = await _db.GetCorrectPercent(socket.Key, sessionName);
                SendMessage(new YourScoreMessage(score, rank, percent), socket.Value);
            };
        }
    }
}