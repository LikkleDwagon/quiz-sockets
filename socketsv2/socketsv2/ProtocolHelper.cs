﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace socketsv2
{
    public static class ProtocolHelpeExtensions
    {
        public static string GetProtocol<PlayModel>(this IHtmlHelper<PlayModel> htmlHelper)
        {
            var protocol = "wss://";
#if DEBUG
            protocol = "ws://";
#endif
            return protocol;
        }
    }
}
