﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace socketsv2.Messages
{
    public class PlayerAnswer : SocketMessage
    {
        public string Player { get; set; }
        public string Option { get; set; }
        public override MessageType Type => MessageType.Answer;
    }
}
