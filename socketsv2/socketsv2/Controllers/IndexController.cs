﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using socketsv2.Messages;
using socketsv2.Models;
using socketsv2.Processors;
using socketsv2.SqlServer;

namespace socketsv2.Controllers
{
    public class HomeController : Controller
    {
        private readonly QuizContext _context;

        public HomeController(QuizContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Play(string sessionName)
        {
            if (string.IsNullOrEmpty(sessionName))
                return NotFound();

            if (!Request.Cookies.ContainsKey("name"))
            {
                return RedirectToAction("Index");
            }

            if (StartupExtensions.GetPlayerSockets(sessionName).ContainsKey(Request.Cookies["name"]))
            {
                if (StartupExtensions.GetPlayerSockets(sessionName).TryRemove(Request.Cookies["name"], out var socket))
                {
                    try
                    {
                        BaseProcessor.SendAll(sessionName, new PlayerDisconnected(Request.Cookies["name"]));
                        await socket.CloseAsync(WebSocketCloseStatus.Empty, "", CancellationToken.None);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }

            var leaderBoard = await _context.GetLeaderboard(sessionName);
            var score = await _context.GetScore(Request.Cookies["name"], sessionName);
            var rank = await _context.GetRank(Request.Cookies["name"], sessionName);
            var percent = await _context.GetCorrectPercent(Request.Cookies["name"], sessionName);
            var connectedPlayers = StartupExtensions.GetPlayerSockets(sessionName).Select(s => s.Key).ToList();
            var model = new PlayerModel() {
                Name = Request.Cookies["name"],
                ConnectedPlayers = connectedPlayers,
                LeaderBoard = leaderBoard,
                Score = score,
                Rank = rank,
                Percent = percent,
                SessionName = sessionName};
            return View("Play", model);
        }

        public async Task<IActionResult> JoinSession()
        {
            if (!Request.Cookies.ContainsKey("name"))
            {
                return RedirectToAction("Index");
            }
            return View("JoinSession");
        }

        public IActionResult Index()
        {
            if(Request.Cookies.ContainsKey("name"))
            {
                return RedirectToAction("JoinSession");
            }
            return View();
        }

        [HttpPost]
        public IActionResult Index(IndexModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Name))
            {
                return RedirectToAction("Index");
            }
            Response.Cookies.Append("name", model.Name, new Microsoft.AspNetCore.Http.CookieOptions() {
                Expires = System.DateTime.Now.AddDays(90)
            });
            return RedirectToAction("JoinSession");
        }
    }
}