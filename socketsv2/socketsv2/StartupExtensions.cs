﻿using System.Collections.Concurrent;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using socketsv2.Messages;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using socketsv2.Messages.Client;
using socketsv2.Processors;

namespace socketsv2
{
    public static class StartupExtensions
    {
        private static ConcurrentDictionary<string, ConcurrentDictionary<string, WebSocket>> PlayerSockets = new ConcurrentDictionary<string, ConcurrentDictionary<string, WebSocket>>();
        public static ConcurrentDictionary<string, WebSocket> HostSockets = new ConcurrentDictionary<string, WebSocket>();
        
        public static IApplicationBuilder UseMySockets(this IApplicationBuilder app)
        {
            return app.Use(async (http, next) =>
                        {
                            if (http.WebSockets.IsWebSocketRequest)
                            {
                                var sessionName = GetSessionSegment(http.Request);
                                
                                if (http.Request.Path.StartsWithSegments("/play"))
                                {
                                    //if(!HostSockets.ContainsKey(sessionName)
                                    var socket = await http.WebSockets.AcceptWebSocketAsync();
                                    if (!PlayerSockets.ContainsKey(sessionName))
                                        PlayerSockets.TryAdd(sessionName, new ConcurrentDictionary<string, WebSocket>());

                                    BaseProcessor.SendAll(sessionName, new PlayerConnected(http.Request.Cookies["name"]));
                                    PlayerSockets[sessionName].TryAdd(http.Request.Cookies["name"], socket);
                                    await OpenSocket(socket, http, PlayerSockets[sessionName], app, sessionName);
                                }

                                if (http.Request.Path.StartsWithSegments("/create"))
                                {
                                    await OpenHostSocket(http, app, sessionName);
                                }
                            }
                            else
                            {
                                await next();
                            }
                        });
        }

        public static string GetSessionSegment(HttpRequest request)
        {
            return request.Path.Value.Split('/').Last().ToString();
        }

        public static ConcurrentDictionary<string, WebSocket> GetPlayerSockets(string sessionName)
        {
            if (PlayerSockets.ContainsKey(sessionName))
                return PlayerSockets[sessionName];

            return new ConcurrentDictionary<string, WebSocket>();
        }

        private static async Task OpenSocket(WebSocket socket, HttpContext http,
            ConcurrentDictionary<string, WebSocket> sockets, IApplicationBuilder builder, string sessionName)
        {
            await Task.Run(async () =>
            {
                sockets.TryAdd(http.Request.Cookies["name"], socket);
                while (socket.State == WebSocketState.Open)
                {
                    await ProcessMessage(http.Request, socket, builder, sessionName);
                }
                sockets.TryRemove(http.Request.Cookies["name"], out socket);
                BaseProcessor.SendAll(sessionName, new PlayerDisconnected(http.Request.Cookies["name"]));
            });
        }

        public static async Task OpenHostSocket(HttpContext http, IApplicationBuilder builder, string sessionName)
        {
            if (HostSockets.ContainsKey(sessionName))
                return;
            var socket = await http.WebSockets.AcceptWebSocketAsync();
            HostSockets.TryAdd(sessionName, socket);
            while (socket.State == WebSocketState.Open)
            {
                await ProcessMessage(http.Request, socket, builder, sessionName);
            }
            HostSockets.TryRemove(sessionName, out _);
        }

        private static async Task ProcessMessage(HttpRequest request, WebSocket socket, IApplicationBuilder builder, string sessionName)
        {
            var message = new byte[1024];
            var received = await socket.ReceiveAsync(message, CancellationToken.None);
            var messageString = System.Uri.UnescapeDataString(Encoding.Default.GetString(message, 0, received.Count));
            if(messageString.Length > 0)
            {
                using (var scope = builder.ApplicationServices.CreateScope())
                {
                    
                    var parsed = JsonConvert.DeserializeObject<Answer>(messageString);
                    if (parsed.Type == ClientMessageType.Answer)
                    {
                        var processor = scope.ServiceProvider.GetService<IProcessor<Answer>>();
                        await processor?.Process(request, parsed, sessionName);
                    }

                    var nextRoundMessage = JsonConvert.DeserializeObject<NextRound>(messageString);
                    if (nextRoundMessage.Type == ClientMessageType.NextRound)
                    {
                        var processor = scope.ServiceProvider.GetService<IProcessor<NextRound>>();
                        await processor?.Process(request, nextRoundMessage, sessionName);
                    }
                }
            }
        }
    }
}
