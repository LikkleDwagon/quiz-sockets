﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace socketsv2.Migrations
{
    public partial class scoresPatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Rounds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date = table.Column<DateTime>(nullable: false),
                    RoundNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rounds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlayerName",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    RoundId = table.Column<int>(nullable: true),
                    RoundId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerName", x => x.Name);
                    table.ForeignKey(
                        name: "FK_PlayerName_Rounds_RoundId",
                        column: x => x.RoundId,
                        principalTable: "Rounds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PlayerName_Rounds_RoundId1",
                        column: x => x.RoundId1,
                        principalTable: "Rounds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerName_RoundId",
                table: "PlayerName",
                column: "RoundId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerName_RoundId1",
                table: "PlayerName",
                column: "RoundId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerName");

            migrationBuilder.DropTable(
                name: "Rounds");
        }
    }
}
