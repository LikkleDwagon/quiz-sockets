﻿namespace socketsv2.Messages.Client
{
    public class NextRound : IClientMessage
    {
        public int RoundNumber { get; set; }
        public string[] Correct { get; set; }
        public ClientMessageType Type { get; set; }
    }
}