﻿using System;
using System.ComponentModel.DataAnnotations;

namespace socketsv2.SqlServer.Models
{
    public class PlayerScore
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int Round { get; set; }
    }
}